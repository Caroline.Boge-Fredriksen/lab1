package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        while(true) {

        System.out.println("Let's play round " + roundCounter);
        String opponentMove= "";
        System.out.println("Your choice (Rock/Paper/Scissors)?");
        String myMove = sc.nextLine();
        
        int rand = (int)(Math.random() * 3);


        
        //bekreft at myMove er gyldig
                if(!rpsChoices.contains(myMove)){
                    while(!rpsChoices.contains(myMove)){
                        System.out.println("I do not understand" + " " + myMove + ". " + "Could you try again?");
                        myMove = sc.nextLine();
                    }
                }

            //generere tilfeldig motstandertrekket (0, 1, 2)
            if(rand == 0){
                opponentMove = "rock";
            } else if(rand == 1){
                opponentMove = "paper";
            } else{
                opponentMove = "scissors";
            }

            //beregne om brukeren vant, tapte eller uavgjort
            if(myMove.equals(opponentMove)){
                System.out.println("Human chose " + myMove + ", computer chose " + opponentMove + ". It's a tied! ");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            } else if((myMove.equals("rock") && opponentMove.equals("scissors")) || (myMove.equals("scissors") && opponentMove.equals("paper")) || (myMove.equals("paper") && opponentMove.equals("rock"))){
                System.out.println("Human chose " + myMove + ", computer chose " + opponentMove + ". Human wins! ");
                humanScore++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }else {
               System.out.println("Human chose " + myMove + ", computer chose " + opponentMove + ". Computer wins! ");
               computerScore++;
               System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }

            roundCounter++;

            //spør om spilleren ønsker å fortsette
            System.out.println("Do you wish to continue playing? (y/n)?");
            String conPlay = sc.nextLine();
            if(conPlay.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }

        }
 
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
